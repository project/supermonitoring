
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits
INTRODUCTION
------------
Monitor your website uptime with www.supermonitoring.com services
- and have the reports displayed in your Drupal panel.

The extension integrates Super Monitoring responsive interface
into Drupal admin panel, enabling you to view reports 
and configure services without the need to leave your CMS 
and log in manually to the external application.

Super Monitoring service main features:
- tests every minute (1440 checks per day!)
- worldwide monitoring network (servers on 4 continents)
- detecting & reporting various outage types
- content checking (keyword search)
- form testing (filling in and submitting)
- performance monitoring (measuring server response times)
- file integrity checking (checksum validation)
- instant alerts (email & SMS)
- downtime history (PDF reports, data export).


INSTALLATION
------------

1. Copy the supermonitoring directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.


MAINTAINERS
-----------
Current maintainers:
* Łukasz Chrzanowski (Deancraft) - https://www.drupal.org/user/3140357

This project has been sponsored by:
 * SITEIMPULSE 
   Visit http://www.siteimpulse.com/ for more information.
