��          t      �              1   %  -   W  m  �  x   �     l     �     �     �      �  I  �       6   -  :   d  �  �  �   8     �  	   �  
   �                   
                                               	    Authorization token Configuration > User Interface > Super Monitoring Go to the configuration and enter your token  If you already have a subscription at www.supermonitoring.com, enter your token to integrate the service with Drupal panel.<br /><br />If you have not an account at www.supermonitoring.com yet, <a href="https://www.supermonitoring.com/?utm_source=Drupal&utm_medium=text&utm_campaign=plugin" target="_blank"><strong>sign up here</strong></a> for a 14-day free trial. Monitor your website uptime with www.supermonitoring.com services - and have the reports displayed in your Drupal panel. Super Monitoring settings Your Account Your Checks Your Contacts https://www.supermonitoring.com/ Project-Id-Version: 
POT-Creation-Date: 2018-04-27 17:17+0200
PO-Revision-Date: 2018-04-27 17:19+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
 Autorisierungs-Token Konfiguration > Benutzeroberfläche > Super Monitoring Gehen Sie zur Konfiguration und geben Sie Ihren Token ein  Wenn Sie bereits ein Abonnement bei www.supermonitoring.de haben, geben Sie Ihr Token ein, um den Dienst in Drupal Panel zu integrieren.<br /><br />Wenn Sie noch kein Konto bei www.supermonitoring.de haben, <a href="https://www.supermonitoring.de/?utm_source=Drupal&utm_medium=text&utm_campaign=plugin" target="_blank"><strong>melden Sie sich hier</strong></a> für eine 14-tägige kostenlose Testversion an. Überwachen Sie die Verfügbarkeit Ihrer Website mit den Services von www.supermonitoring.de - und lassen Sie die Berichte in Ihrem Drupal-Panel anzeigen. Super Monitoring Konfiguration Ihr Konto Ihre Tests Ihre Kontakte https://www.supermonitoring.de/ 