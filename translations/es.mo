��          t      �              1   %  -   W  m  �  x   �     l     �     �     �      �  I  �       7   /  '   g  �  �  �        �  	   �     �     �     �         
                                               	    Authorization token Configuration > User Interface > Super Monitoring Go to the configuration and enter your token  If you already have a subscription at www.supermonitoring.com, enter your token to integrate the service with Drupal panel.<br /><br />If you have not an account at www.supermonitoring.com yet, <a href="https://www.supermonitoring.com/?utm_source=Drupal&utm_medium=text&utm_campaign=plugin" target="_blank"><strong>sign up here</strong></a> for a 14-day free trial. Monitor your website uptime with www.supermonitoring.com services - and have the reports displayed in your Drupal panel. Super Monitoring settings Your Account Your Checks Your Contacts https://www.supermonitoring.com/ Project-Id-Version: 
POT-Creation-Date: 2018-04-27 17:17+0200
PO-Revision-Date: 2018-04-27 17:19+0200
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
 Token de autorización Configuración > Interfaz de usuario > Super Monitoring Ve a configuración e ingresa tu token  Si ya tiene una suscripción a www.supermonitoring.es, introduzca su token de integrar el servicio con el panel de Drupal.<br /><br />Si usted no tiene una cuenta en www.supermonitoring.es todavía, <a href="https://www.supermonitoring.es/?utm_source=Drupal&utm_medium=text&utm_campaign=plugin" target="_blank"><strong>regístrese aquí</strong></a> para una prueba gratuita de 14 días. Supervise el tiempo de actividad de su blog con los servicios de www.supermonitoring.ed y haga que todos los gráficos y tablas aparezcan en su panel de Drupal. Super Monitoring configuración Su cuenta Sus pruebas Sus contactos https://www.supermonitoring.es/ 