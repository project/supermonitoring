��          t      �              1   %  -   W  m  �  x   �     l     �     �     �      �  B  �       8   %  1   ^  q  �          �     �     �     �     �         
                                               	    Authorization token Configuration > User Interface > Super Monitoring Go to the configuration and enter your token  If you already have a subscription at www.supermonitoring.com, enter your token to integrate the service with Drupal panel.<br /><br />If you have not an account at www.supermonitoring.com yet, <a href="https://www.supermonitoring.com/?utm_source=Drupal&utm_medium=text&utm_campaign=plugin" target="_blank"><strong>sign up here</strong></a> for a 14-day free trial. Monitor your website uptime with www.supermonitoring.com services - and have the reports displayed in your Drupal panel. Super Monitoring settings Your Account Your Checks Your Contacts https://www.supermonitoring.com/ Project-Id-Version: 
POT-Creation-Date: 2018-04-27 17:17+0200
PO-Revision-Date: 2018-04-27 17:18+0200
Last-Translator: 
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-Basepath: ../src
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: t
X-Poedit-SearchPath-0: Controller/SupermonitoringController.php
X-Poedit-SearchPath-1: Form/SupermonitoringSettingsForm.php
X-Poedit-SearchPath-2: dodatkowe.php
 Token autoryzacyjny Konfiguracja > Interfejs użytkownika > Super Monitoring Przejdź do konfiguracji i wprowadź swój token  Jeśli posiadasz subskrypcję na www.supermonitoring.pl, wprowadź swój token, żeby zintegrować usługę z Drupalem.<br /><br />Jeżeli nie masz jeszcze konta na www.supermonitoring.pl, <a href="https://www.supermonitoring.pl/?utm_source=Drupal&utm_medium=text&utm_campaign=plugin" target="_blank"><strong>zarejestruj</strong></a> bezpłatne 14-dniowe konto próbne. Monitoruj dostępność swojej strony za pomocą usług www.supermonitoring.pl - i wyświetlaj raporty wewnątrz panelu Drupal. Ustawienia Super Monitoringu Twoje konto Twoje testy Twoje kontakty https://www.supermonitoring.pl/ 